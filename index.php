<?php

    include_once ("_common/header.php")

?>

<html>

<!--Materialize CSS import-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
<link rel="stylesheet" href="_common/styles.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>


<header>
    <title>Doublepump.RIP - Fortnite</title>

    <h1 align="center">Fortnite Stats</h1>

</header>

<?php



?>

<body bgcolor="#E8E8E8">

<!--Adding space after header\-->
    <br><br><br>


    <div class="row">

        <!--  alignment start -->
        <div class="col s3"></div>
        <!--  alignment end -->

        <form action="_common/getinfo.php">

        <div class="col s3 offset-l1">

            <input type="text" placeholder="Enter Fortnite Username">

        </div>
        <div class="col s3">

            <button class="btn" id="find_stats_btn" type="submit" name="action">Find Stats</button>



        </div>

        </form>

        <!--  alignment start -->
        <div class="col s1"></div>
        <!--  alignment end -->
    </div>

</body>

</html>